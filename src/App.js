import React, {Component} from 'react';
import router from './router';
import {
    BrowserRouter,
    Switch,
    Route
} from 'react-router-dom';

import Header from './layouts/Header'
import Footer from './layouts/Footer'


class App extends Component {
    render() {
        return (
            <BrowserRouter>
                <Switch>
                    <div>
                        <Header/>

                        {
                            router.map(( item, key ) =>
                                <Route exact path={ item.path } component={ item.component } key={ key }/>
                            )
                        }

                        <Footer/>
                    </div>
                </Switch>
            </BrowserRouter>
        )
    }
}

export default App
