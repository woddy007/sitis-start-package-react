import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import './styles/uikit.theme.less'

ReactDOM.render(<App />, document.getElementById('root'));
